LOCAL_PATH:= $(call my-dir)
SRC_PATH:= $(LOCAL_PATH)/i2c-tools

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := eng
LOCAL_C_INCLUDES += $(SRC_PATH)/include $(LOCAL_PATH)/$(KERNEL_DIR)/include
LOCAL_SRC_FILES := i2c-tools/lib/smbus.c
LOCAL_MODULE := i2c
LOCAL_EXPORT_C_INCLUDE_DIRS := $(LOCAL_PATH)/i2c-tools/include
include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := eng debug optional
LOCAL_C_INCLUDES += $(SRC_PATH) $(SRC_PATH)/include $(LOCAL_PATH)/$(KERNEL_DIR)/include
LOCAL_SRC_FILES := i2c-tools/tools/i2cbusses.c i2c-tools/tools/util.c i2c-tools/lib/smbus.c
LOCAL_MODULE := i2c-tools
LOCAL_STATIC_LIBRARIES := i2c
include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := eng debug optional
LOCAL_SRC_FILES:=i2c-tools/tools/i2cdetect.c
LOCAL_MODULE:=i2cdetect
LOCAL_CPPFLAGS += -DANDROID
LOCAL_SHARED_LIBRARIES:=libc
LOCAL_STATIC_LIBRARIES := i2c-tools i2c
LOCAL_C_INCLUDES += $(SRC_PATH) $(SRC_PATH)/include $(LOCAL_PATH)/$(KERNEL_DIR)/include
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := eng debug optional
LOCAL_SRC_FILES:=i2c-tools/tools/i2cget.c
LOCAL_MODULE:=i2cget
LOCAL_CPPFLAGS += -DANDROID
LOCAL_SHARED_LIBRARIES:=libc
LOCAL_STATIC_LIBRARIES := i2c-tools i2c
LOCAL_C_INCLUDES += $(SRC_PATH) $(SRC_PATH)/include $(LOCAL_PATH)/$(KERNEL_DIR)/include
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := eng debug optional
LOCAL_SRC_FILES:=i2c-tools/tools/i2cset.c
LOCAL_MODULE:=i2cset
LOCAL_CPPFLAGS += -DANDROID
LOCAL_SHARED_LIBRARIES:=libc
LOCAL_STATIC_LIBRARIES := i2c-tools i2c
LOCAL_C_INCLUDES += $(SRC_PATH) $(SRC_PATH)/include $(LOCAL_PATH)/$(KERNEL_DIR)/include
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := eng debug optional
LOCAL_SRC_FILES:=i2c-tools/tools/i2cdump.c
LOCAL_MODULE:=i2cdump
LOCAL_CPPFLAGS += -DANDROID
LOCAL_SHARED_LIBRARIES:=libc
LOCAL_STATIC_LIBRARIES := i2c-tools i2c
LOCAL_C_INCLUDES += $(SRC_PATH) $(SRC_PATH)/include $(LOCAL_PATH)/$(KERNEL_DIR)/include
include $(BUILD_EXECUTABLE)